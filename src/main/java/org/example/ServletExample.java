package org.example;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletExample
 */
@WebServlet("/ServletExample/*")
public class ServletExample extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ServletExample() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String pathInfo = request.getPathInfo();
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
		String methodType = null;
        // Verifica se ci sono path variables e processale
        if (pathInfo != null && pathInfo.length() > 1) {
            // Rimuovi il separatore iniziale "/"
            String[] pathParts = pathInfo.substring(1).split("/");

            methodType = pathParts[pathParts.length-1];
        }
         
				
        out.println(methodType + " - Served at: "+(request.getContextPath()));
		out.println("<button><a href=\"../index.jsp\">Indietro</a></button>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
